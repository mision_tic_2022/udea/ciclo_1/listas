
'''
under_score = separado por guión bajo
camelCase = minúscula y mayúscula
'''
#crear una lista
mi_lista = [100, 50, 400, "hola mundo", True, False, 3.1416]
#Acceder a un elemento de la lista
print(mi_lista[3])
#Obtener el tamaño de la lista
tamanio = len(mi_lista)
print(tamanio)

ultimo_elemento = mi_lista[ tamanio-1 ]
print(ultimo_elemento)

#Añadir nuevos elementos a la lista
mi_lista.append('Otro elemento')
mi_lista.append(10)

tamanio = len(mi_lista)
print(f'Nuevo tamaño -> {tamanio}')

print("Último elemento -> ", mi_lista[ tamanio-1 ])

#Eliminar un elemento
mi_lista.pop()
tamanio = len(mi_lista)
print("Último elemento -> ", mi_lista[tamanio-1])

#Recorrer lista
for i in mi_lista:
    print(i)



#Iterar n veces
for i in range(0, 10):
    print(i)

#Reunión meet: https://meet.google.com/ucj-pthy-ddz